package basico;

public class Cliente {
	
	String nome;
	double salario;
	String numeroDaConta;
	
	
	public Cliente(String nome, double salario, String numeroDaConta) {
		super();
		this.nome = nome;
		this.salario = salario;
		this.numeroDaConta = numeroDaConta;
	}
		
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public String getNumeroDaConta() {
		return numeroDaConta;
	}
	public void setNumeroDaConta(String numeroDaConta) {
		this.numeroDaConta = numeroDaConta;
	}
	
	
	
}
